package pl.edu.pwsztar.domain.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PeselValidator {

    public static boolean isValid(final String pesel) {

        if(pesel == null) {
            return false;
        }

        String regex = "^[0-9]{11}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pesel);

        if (!matcher.matches()) {
            return false;
        }

        int check = 0;
        int[] peselArr = new int[11];
        for (int i = 0; i < 11; i++) {
            peselArr[i] = Integer.parseInt(pesel.substring(i, i + 1));
        }

        int[] weights = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };

        for (int i = 0; i < 10; i++) {
            check += weights[i] * peselArr[i];
        }

        int lastNumber = check % 10;
        int controlNumber = 10 - lastNumber;

        return controlNumber == peselArr[10];
    }
}
